-- !Ups
ALTER TABLE "product"
ADD "price" FLOAT;

-- !Downs
ALTER TABLE "product"
DROP COLUMN "price";