package models

import org.joda.time.DateTime
import play.api.libs.json.{Json, OFormat}

case class Order(id: Long, totalPrice: Float, date: DateTime, products: Seq[Product], userId: Long)

object Order {
  implicit val productFormat: OFormat[Order] = Json.format[Order]
}
