package models

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserDataRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)
                                  (implicit ec: ExecutionContext) {

  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class UserDataTable(tag: Tag) extends Table[UserData](tag, "UserData") {
    def id = column[Long]("id", O.AutoInc, O.PrimaryKey)

    def name = column[String]("name")

    def surname = column[String]("surname")

    def address = column[String]("address")

    def city = column[String]("city")

    def phoneNumber = column[String]("phoneNumber")

    override def * = (id, name, surname, address, city, phoneNumber) <> ((UserData.apply _).tupled, UserData.unapply)
  }

  val userData = TableQuery[UserDataTable]

  def list(): Future[Seq[UserData]] = db.run {
    userData.result
  }

  def create(name: String, surname: String, address: String, city: String, phoneNumber: String): Future[UserData] = db.run {
    (userData.map(ud => (ud.name, ud.surname, ud.address, ud.city, ud.phoneNumber))
      returning userData.map(_.id)
      into { case ((name, surname, address, city, phoneNumber), id) => UserData(id, name, surname, address, city, phoneNumber) }
      ) += (name, surname, address, city, phoneNumber)
  }

  def update(id: Long, newUserData: UserData): Future[Unit] = {
    val userDataToUpdate: UserData = newUserData.copy(id)
    db.run(userData.filter(_.id === id).update(userDataToUpdate).map(_ => ()))
  }

  def delete(id: Long): Future[Unit] = db.run(userData.filter(_.id === id).delete.map(_ => ()))

  def getById(id: Long): Future[UserData] = db.run {
    userData.filter(_.id === id).result.head
  }

}
