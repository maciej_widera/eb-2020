package models

import play.api.libs.json.{Json, OFormat}

case class OrderProduct(id: Long, orderId: Long, productId: Long)


object OrderProduct {
  implicit val productFormat: OFormat[OrderProduct] = Json.format[OrderProduct]
}
