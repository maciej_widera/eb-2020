package models

import play.api.libs.json.{Json, OFormat}

case class User(id: Long, login: String, password: String,
                email: String, userDataId: Long)

object User {
  implicit val userFormat: OFormat[User] = Json.format[User]
}