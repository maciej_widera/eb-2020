package models

import play.api.libs.json.{Json, OFormat}

case class UserData(id: Long, name: String, surname: String,
                    address: String, city: String, phoneNumber: String)

object UserData {
  implicit val userDataFormat: OFormat[UserData] = Json.format[UserData]
}