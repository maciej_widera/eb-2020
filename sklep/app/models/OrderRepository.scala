package models

import java.sql.Date

import javax.inject.Inject
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class OrderRepository @Inject()(dbConfigProvider: DatabaseConfigProvider, userRepository: UserRepository)
                               (implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private class OrderTable(tag: Tag) extends Table[Order](tag, "order") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def totalPrice = column[Float]("total_price")

    def userId = column[Long]("user_id")

    def date = column[Date]("date")

    def user_id_fk = foreignKey("user_fk", userId, user)

    override def * = (id, totalPrice, userId, date) <> ((Order.apply _).tupled, Order.unapply)
  }

  private class OrderProductTable(tag: Tag) extends Table[OrderProduct](tag, "order_product") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def orderId = column[Long]("order_id")

    def productId = column[Long]("prod_id")

    override def * = (id, orderId, productId) <> ((OrderProduct.apply _).tupled, OrderProduct.unapply)
  }

  import userRepository.UserTable

  val user = TableQuery[UserTable]

  def create(totalPrice: Float,
             date: DateTime, products: Seq[Product],
             userId: Long): Future[Order] = db.run {

  }

}
