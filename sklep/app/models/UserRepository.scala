package models

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserRepository @Inject()(dbConfigProvider: DatabaseConfigProvider, userDataRepository: UserDataRepository)
                              (implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class UserTable(tag: Tag) extends Table[User](tag, "User") {
    def id = column[Long]("id", O.AutoInc, O.PrimaryKey)

    def login = column[String]("login", O.Unique)

    def password = column[String]("password")

    def email = column[String]("email", O.Unique)

    def userDataId = column[Long]("user_data_id")

    def userData_fk = foreignKey("user_data_fk", userDataId, userData)(_.id)

    override def * = (id, login, password, email, userDataId) <> ((User.apply _).tupled, User.unapply)
  }

  import userDataRepository.UserDataTable

  private val userData = TableQuery[UserDataTable]
  private val user = TableQuery[UserTable]

  def create(login: String, password: String, email: String, userDataId: Long): Future[User] = db.run {
    (user.map(u => (u.login, u.password, u.email, u.userDataId))
      returning user.map(_.id)
      into { case ((login, password, email, userDataId), id) => User(id, login, password, email, userDataId) }
      ) += (login, password, email, userDataId)
  }

  def update(id: Long, newUser: User): Future[Unit] = {
    val userToUpdate = newUser.copy(id)
    db.run(user.filter(_.id === id).update(userToUpdate)).map(_ => ())
  }

  def delete(id: Long): Future[Unit] = db.run(user.filter(_.id === id).delete.map(_ => ()))

  def getById(id: Long): Future[User] = db.run {
    user.filter(_.id === id).result.head
  }

  def list(): Future[Seq[User]] = db.run {
    user.result
  }

  def getByLogin(login: String): Future[User] = db.run {
    user.filter(_.login === login).result.head
  }


}
