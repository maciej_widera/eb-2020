package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class ProductController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def getAllProducts: Action[AnyContent] = Action {
    Ok("GetProducts")
  }

  def getProductById(id: Long): Action[AnyContent] = Action {
    Ok(s"Get ${id}")
  }

  def addProduct(): Action[AnyContent] = Action {
    Ok("addedProduct")
  }

  def editProductById(id: Long): Action[AnyContent] = Action {
    Ok(s"updated ${id}")
  }

  def deleteProductById(id: Long): Action[AnyContent] = Action {
    Ok(s"Deleted: ${id}")
  }

}
