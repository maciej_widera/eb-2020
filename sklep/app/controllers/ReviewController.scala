package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class ReviewController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addReview(): Action[AnyContent] = Action {
    Ok("")
  }

  def getAllReviews: Action[AnyContent] = Action {
    Ok("")
  }

  def getReview(id: Long): Action[AnyContent] = Action {
    Ok("Item")
  }

  def editReview(): Action[AnyContent] = Action {
    Ok("")
  }

  def deleteReview(): Action[AnyContent] = Action {
    Ok("Deleted")
  }

}
