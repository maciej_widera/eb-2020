package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class DealController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addDeal(): Action[AnyContent] = Action {
    Ok("")
  }

  def getAllDeals: Action[AnyContent] = Action {
    Ok("")
  }

  def getDeal: Action[AnyContent] = Action {
    Ok("Item")
  }

  def editDeal(): Action[AnyContent] = Action {
    Ok("")
  }

  def deleteDeal(): Action[AnyContent] = Action {
    Ok("Deleted")
  }

}
