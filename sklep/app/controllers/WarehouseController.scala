package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class WarehouseController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addItem(): Action[AnyContent] = Action {
    Ok("Added item")
  }

  def deleteItem(): Action[AnyContent] = Action {
    Ok("Deleted")
  }

  def editItem(): Action[AnyContent] = Action {
    Ok("Edited")
  }

  def getAllItems: Action[AnyContent] = Action {
    Ok("Items")
  }

  def getItem: Action[AnyContent] = Action {
    Ok("Item")
  }

}
