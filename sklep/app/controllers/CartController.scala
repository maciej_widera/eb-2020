package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class CartController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addItemToCart(): Action[AnyContent] = Action {
    Ok("Added item")
  }

  def getItemsFromCart: Action[AnyContent] = Action {
    Ok("Items")
  }

  def deleteItemFromCartById(id: Long): Action[AnyContent] = Action {
    Ok(s"Deleted id: ${id}")
  }

  def modifyItemFromCartById(id: Long): Action[AnyContent] = Action {
    Ok(s"Modified id: ${id}")
  }

}
