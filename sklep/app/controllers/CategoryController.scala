package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class CategoryController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def getAllCategories: Action[AnyContent] = Action {
    Ok("Categories")
  }

  def getCategoryByName(name: String): Action[AnyContent] = Action {
    Ok(s"Category: ${name}")
  }

  def addCategory(): Action[AnyContent] = Action {
    Ok("Category added")
  }

  def editCategoryByName(name: String): Action[AnyContent] = Action {
    Ok(s"Category edited: ${name}")
  }

  def deleteCategoryByName(name: String): Action[AnyContent] = Action {
    Ok(s"Category deleted: ${name}")
  }

}
