package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class UserController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addUser(): Action[AnyContent] = Action {
    Ok("Added user")
  }

  def editUserById(id: Long): Action[AnyContent] = Action {
    Ok(s"update user ${id}")
  }

  def getAllUsers: Action[AnyContent] = Action {
    Ok("get users")
  }

  def getUserById(id: Long): Action[AnyContent] = Action {
    Ok(s"User id: ${id}")
  }

  def deleteUserById(id: Long): Action[AnyContent] = Action {
    Ok(s"Deleted id: ${id}")
  }

}
