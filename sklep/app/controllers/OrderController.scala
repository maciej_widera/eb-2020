package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class OrderController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def getAllOrders: Action[AnyContent] = Action {
    Ok("Orders")
  }

  def getOrderById(id: Long): Action[AnyContent] = Action {
    Ok(s"Order ${id}")
  }

  def addOrder(): Action[AnyContent] = Action {
    Ok("Added order")
  }

  def deleteOrderById(id: Long): Action[AnyContent] = Action {
    Ok(s"Deleted order: ${id}")
  }

  def editOrderById(id: Long): Action[AnyContent] = Action {
    Ok(s"Edited order: ${id}")
  }
}
