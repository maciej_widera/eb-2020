package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class ShippingController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def addShippingMethod(): Action[AnyContent] = Action {
    Ok("")
  }

  def editShippingMethod(): Action[AnyContent] = Action {
    Ok("")
  }

  def deleteShippingMethod(): Action[AnyContent] = Action {
    Ok("")
  }

  def getAllShippingMethods: Action[AnyContent] = Action {
    Ok("")
  }

  def getShippingMethod(id: Long): Action[AnyContent] = Action {
    Ok("")
  }

}
