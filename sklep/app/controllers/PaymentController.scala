package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class PaymentController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def getAllPayments: Action[AnyContent] = Action {
    Ok("Payments")
  }

  def getPaymentById(id: Long): Action[AnyContent] = Action {
    Ok(s"Payment: ${id}")
  }

  def createPayment(): Action[AnyContent] = Action {
    Ok(s"Payment created")
  }

  def deletePaymentById(id: Long): Action[AnyContent] = Action {
    Ok(s"Payment deleted ${id}")
  }

  def editPaymentById(id: Long): Action[AnyContent] = Action {
    Ok(s"Edited payment: ${id}")
  }

}
